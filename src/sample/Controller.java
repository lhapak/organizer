package sample;

import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import sample.database.DataBase;

import java.sql.SQLException;
import java.time.LocalDate;

public class Controller {
    public Pane newEventWindow;
    public Pane mainWindow;
    public Button addButton;
    public Button editButton;
    public TableView<Event> eventTableView;

    public TextField newEventName;
    public TextArea newEventDescription;
    public DatePicker newEventDate;

    public TextField searchBar;
    public Label invalidNameWarning;
    public Label invalidDateWarning;
    public Label eventNotSelectedWarning;

    private Event selectedEvent;
    private DataBase db = new DataBase();

    public void initialize() {
        db.createTable();
        searchBar.textProperty().addListener(observable -> refresh());
        refresh();
    }

    public void addNewEvent() {
        if (isFormValid()) {
            Event event = new Event(newEventName.getText(), newEventDescription.getText(), newEventDate.getValue().toString());
            try {
                if (db.isNotPresentInDb(event)) {
                    db.addNewEvent(event);
                    closeNewEventWindow();
                    invalidNameWarning.setText("");
                } else {
                    invalidNameWarning.setText("Takie wydarzenie zostało już dodane");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void openNewEventWindow() {
        mainWindow.setVisible(false);
        newEventWindow.setVisible(true);
    }

    public void deleteEvent() {
        if (eventTableView.getSelectionModel().getSelectedItem() != null) {
            eventNotSelectedWarning.setText("");
            selectedEvent = eventTableView.getSelectionModel().getSelectedItem();
            db.deleteEvent(selectedEvent.getId());
            refresh();
        } else {
            eventNotSelectedWarning.setText("Wybierz wydarzenie które chcesz usunąć");
        }
    }

    public void editEvent() {
        if (eventTableView.getSelectionModel().getSelectedItem() != null) {
            eventNotSelectedWarning.setText("");
            addButton.setVisible(false);
            editButton.setVisible(true);
            selectedEvent = eventTableView.getSelectionModel().getSelectedItem();
            openNewEventWindow();
            newEventName.setText(selectedEvent.getName());
            newEventDescription.setText(selectedEvent.getDescription());
            String[] date = selectedEvent.getDate().split("-");
            newEventDate.setValue(LocalDate.of(Integer.parseInt(date[0]), Integer.parseInt(date[1]), Integer.parseInt(date[2])));
        } else {
            eventNotSelectedWarning.setText("Wybierz wydarzenie które chcesz edytować");
        }
    }

    public void confirmEdit() {
        if (isFormValid()) {
            Event event = new Event(newEventName.getText(), newEventDescription.getText(), newEventDate.getValue().toString());
            try {
                if (db.isNotPresentInDb(event)) {
                    db.updateEvent(selectedEvent.getId(), event);
                    invalidNameWarning.setText("");
                    closeNewEventWindow();
                } else {
                    invalidNameWarning.setText("Takie wydarzenie zostało już dodane");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void closeNewEventWindow() {
        mainWindow.setVisible(true);
        newEventWindow.setVisible(false);
        addButton.setVisible(true);
        editButton.setVisible(false);
        clearInput();
        refresh();
    }

    private void refresh() {
        eventTableView.setItems(db.getAllEvents(searchBar.getText()));
    }

    private void clearInput() {
        newEventName.setText("");
        newEventDescription.setText("");
        newEventDate.setValue(null);
        invalidDateWarning.setText("");
        invalidNameWarning.setText("");
        eventNotSelectedWarning.setText("");
    }

    private boolean isFormValid() {
        if (newEventName.getText().equals("")) {
            invalidNameWarning.setText("Podaj nazwe wydarzenia");
            return false;
        } else {
            invalidNameWarning.setText("");
        }
        if (newEventDate.getValue() == null) {
            invalidDateWarning.setText("Podaj datę");
            return false;
        } else {
            invalidDateWarning.setText("");
        }
        return true;
    }
}
