package sample;

public class Event {
    private String id;
    private String name;
    private String description;
    private String date;

    public Event(String id, String name, String description, String date) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.date = date;
    }

    Event(String name, String description, String date) {
        this.name = name;
        this.description = description;
        this.date = date;
    }

    String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getDate() {
        return date;
    }
}
