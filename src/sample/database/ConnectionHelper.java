package sample.database;

import java.sql.*;

class ConnectionHelper {

    private static Connection con;
    private static Statement statement;
    private static Connection connection = getDbConnection();

    static void executeUpdate(String sql) {
        try {
            statement = connection.createStatement();
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    static ResultSet executeQuery(String sql) {
        ResultSet resultSet = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSet;
    }

    private static Connection getDbConnection() {

        String connectionUrl = "jdbc:mysql://localhost:3306/organizer?serverTimezone=UTC";

        try {
            con = DriverManager.getConnection(connectionUrl, "demo", "password");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return con;
    }
}