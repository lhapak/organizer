package sample.database;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sample.Event;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DataBase {

    private ObservableList<Event> eventList = FXCollections.observableArrayList();

    public void createTable() {
        String sql = "create table if not exists Events(\n" +
                "id int primary key auto_increment,\n" +
                "name varchar(30),\n" +
                "description varchar(100),\n" +
                "date varchar(10) not null\n" +
                ");";
        ConnectionHelper.executeUpdate(sql);
    }

    public boolean isNotPresentInDb(Event event) throws SQLException {
        String sql = String.format("Select count(*) from Events where name = '%s' and Description = '%s' and Date = '%s'",
                event.getName(), event.getDescription(), event.getDate());
        ResultSet rs = ConnectionHelper.executeQuery(sql);
        boolean eventNotExist = true;
        if (rs.next()) {
            if (rs.getInt(1) > 0) {
                eventNotExist = false;
            }
        }
        return eventNotExist;
    }

    public ObservableList<Event> getAllEvents(String filter) {
        eventList.clear();
        ResultSet rs;
        try {
            rs = getEvents(filter);
            while (rs.next()) {
                eventList.add(new Event(rs.getString(1), rs.getString(2),
                        rs.getString(3), rs.getString(4)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return eventList;
    }

    public void addNewEvent(Event event) {
        String sql = String.format("Insert Into Events (name, description, date) values('%s', '%s', '%s')",
                event.getName(), event.getDescription(), event.getDate());
        ConnectionHelper.executeUpdate(sql);
    }

    public void deleteEvent(String id) {
        String sql = String.format("Delete from Events Where Id='%s'", id);
        ConnectionHelper.executeUpdate(sql);
    }

    public void updateEvent(String id, Event event) {
        String sql = String.format("Update Events Set Name='%s', Description='%s', Date='%s' Where Id='%s'",
                event.getName(), event.getDescription(), event.getDate(), id);
        ConnectionHelper.executeUpdate(sql);
    }

    private ResultSet getEvents(String filter) {
        if (filter.length() == 0) {
            return ConnectionHelper.executeQuery("Select * from Events Order By Date ASC");
        } else {
            String sql = String.format("Select * from Events where name like '%%%s%%' or description like'%%%s%%' " +
                            "or date like'%%%s%%' Order By Date ASC",
                    filter, filter, filter);
            return ConnectionHelper.executeQuery(sql);
        }
    }
}